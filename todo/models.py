from django.db import models


class Task(models.Model):
    class Status(models.TextChoices):
        In_progress = 'In progress'
        Done = 'Done'
        Pending = 'Pending'

    title = models.CharField(max_length=100, null=False, blank=False, verbose_name='Title')
    description = models.CharField(max_length=500, null=True, blank=True, verbose_name='Description')
    status = models.CharField(max_length=50, choices=Status.choices, default=Status.In_progress, verbose_name='Status')
    created_at = models.DateTimeField(auto_now_add=True, verbose_name='Creation date and time')

    class Meta:
        verbose_name = 'Task'
        verbose_name_plural = 'Tasks'

    def __str__(self):
        return f"{self.pk}. {self.title}"