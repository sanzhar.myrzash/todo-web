from django import forms
from .models import Task


class TaskForm(forms.ModelForm):
    class Meta:
        model = Task
        fields = '__all__'
        widgets = {
            'title': forms.TextInput(attrs={
                'class': 'form-control',
                'placeholder': 'Title'}),
            'description': forms.Textarea(attrs={
                'class': 'form-control',
                'rows': 5}),
            'status': forms.Select(attrs={
                'class': 'form-select',
                'placeholder': 'Status'
            })
        }


class SearchForm(forms.Form):
    search = forms.CharField(max_length=50, required=False, label='Search')
