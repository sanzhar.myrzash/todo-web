from django.shortcuts import render
from django.views.generic import ListView

from todo.models import Task


class IndexView(ListView):
    template_name = 'index.html'
    model = Task
    context_object_name = 'tasks'
